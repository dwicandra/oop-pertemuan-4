/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ebc;

/**
 *
 * @author Dwicandra
 */
public class Mahasiswa {
    private String nimMhs;
    private String namaMhs;
    private int tinggiMhs;
    private boolean statusPindahanMhs;
    Mahasiswa(){}
    Mahasiswa(String nim, String nama, int tinggi, boolean isPindahan){
        nimMhs = nim;
        namaMhs = nama;
        tinggiMhs = tinggi;
        setPindahan(isPindahan);
    }
    
    public void setPindahan(boolean b) {
        statusPindahanMhs = b;
    }
    
    public String getNim() {
        return nimMhs;
    }
    public String getNama() {
        return namaMhs;
    }
    public int getTinggi() {
        return tinggiMhs;
    }
    public String getStatusPindahan() {
        if(statusPindahanMhs){
            return "Ya";
        } else {
            return "Tidak";
        }
    }
}
