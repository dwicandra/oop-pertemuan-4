## OBJECT BOUNDARY

# Object Entity / Entitas
Padanannya adalah table di database.

# Boundary
Contoh: kelas layar, IF (file I/O).

# Controls
Untuk mediasi antara boundary dan entitas. Mengatur eksekusi perintah yang datang dari batas dengan berinteraksi dengan entitas dan object boundary.
